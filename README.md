### What is this repository for? ###

Page Object Model framework for automation testing MyStore App using Java and Selenium

automationpractice.com the sample real-time ecommerce website for automation testing practice.

# Technologies:

* Selenium Web Driver

* Java

* Javafaker

* Maven

* jUnit

* Page object pattern

* Page factory

# Requirements
1.	Java – The Automation Tester will need experience in this programming language. 
2.	Maven – This is a build automation tool used primarily for Java projects.
3.	Download the Intellij IDE
4.	Java Development Kit (JDK) – Download the zip file under the ‘Link’ column and in the row of “Binary zip archive” – https://maven.apache.org/download.cgi
5.	Git – see the next section for more – “Where to find the code and how to manage it”
6.	Install the following browsers: Firefox and Chrome 

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact